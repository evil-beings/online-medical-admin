# 在线医疗小程序后端

#### 介绍
Springboot+Mybatis-plus+Redis+WebSocket在线医疗小程序后端。

#### 软件架构
软件架构说明

#### 开发环境
1.  idea
2.  mysql
3.  redis
4.  Git
5.  maven




#### 框架支持

1.  Spring Boot
2.  mybatis-plus
3.  spring security+JWT
4.  redis


#### 使用说明
#### Git 在idea使用
1. 下载安装Git
进入到Git官网的下载地址：https://git-scm.com/downloads
2. 下载远程仓库：点击导航栏VCS>>从版本控制获取>>粘贴从gitee上仓库的url(在gitee仓库点克隆把连接复制过来)
3. 上传本地更新：导航栏找到Git先提交后推送
#### 参与贡献

