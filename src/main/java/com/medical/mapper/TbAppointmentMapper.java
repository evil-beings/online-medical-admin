package com.medical.mapper;

import com.medical.model.TbAppointment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author ziluolan
* @description 针对表【tb_appointment】的数据库操作Mapper
* @createDate 2023-06-10 11:00:28
* @Entity com.medical.model.TbAppointment
*/
public interface TbAppointmentMapper extends BaseMapper<TbAppointment> {

}




