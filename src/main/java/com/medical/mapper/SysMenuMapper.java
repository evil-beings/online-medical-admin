package com.medical.mapper;

import com.medical.model.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author ziluolan
* @description 针对表【sys_menu】的数据库操作Mapper
* @createDate 2023-05-11 11:25:48
* @Entity com.medical.model.SysMenu
*/
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}




