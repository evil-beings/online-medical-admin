package com.medical.mapper;

import com.medical.model.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author ziluolan
* @description 针对表【sys_user】的数据库操作Mapper
* @createDate 2023-05-09 21:35:41
* @Entity com.medical.model.SysUser
*/
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> getCloudList(List<Integer> list);

    List<SysUser> grade();
}




