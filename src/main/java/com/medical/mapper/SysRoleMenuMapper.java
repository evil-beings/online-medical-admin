package com.medical.mapper;

import com.medical.model.SysMenu;
import com.medical.model.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
* @author ziluolan
* @description 针对表【sys_role_menu】的数据库操作Mapper
* @createDate 2023-05-11 11:25:58
* @Entity com.medical.model.SysRoleMenu
*/
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {


}




