package com.medical.mapper;

import com.medical.model.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author ziluolan
* @description 针对表【sys_user_role】的数据库操作Mapper
* @createDate 2023-05-09 21:34:22
* @Entity com.medical.model.SysUserRole
*/
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}




