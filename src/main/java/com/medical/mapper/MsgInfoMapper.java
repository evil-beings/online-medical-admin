package com.medical.mapper;

import com.medical.model.MsgInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author ziluolan
* @description 针对表【msg_info(消息表)】的数据库操作Mapper
* @createDate 2023-05-13 21:40:46
* @Entity com.medical.model.MsgInfo
*/
public interface MsgInfoMapper extends BaseMapper<MsgInfo> {

    List<MsgInfo> selectMsgList(@Param("fromUserId") Integer fromUserId, @Param("toUserId") Integer toUserId);

    void msgRead(@Param("fromUserId") Integer fromUserId, @Param("toUserId") Integer toUserId);

    void delMsgById(Integer id);
}




