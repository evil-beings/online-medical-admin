package com.medical.mapper;

import com.medical.model.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author ziluolan
* @description 针对表【sys_role】的数据库操作Mapper
* @createDate 2023-05-09 21:33:58
* @Entity com.medical.model.SysRole
*/
public interface SysRoleMapper extends BaseMapper<SysRole> {

}




