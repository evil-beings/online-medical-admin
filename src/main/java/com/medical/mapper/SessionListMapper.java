package com.medical.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.medical.model.SessionList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author ziluolan
* @description 针对表【session_list(会话列表)】的数据库操作Mapper
* @createDate 2023-05-13 21:40:46
* @Entity com.medical.model.SessionList
*/
public interface SessionListMapper extends BaseMapper<SessionList> {

    void addUnReadCount(@Param("userId") Integer userId, @Param("toUserId") Integer toUserId);

    Integer selectIdByUser(@Param("fromId") Integer fromId,@Param("toId") Integer toId);

    List<Integer> selectUserIdByUserId(Integer id);

    void delUnReadCount(@Param("fromUserId") Integer fromUserId,@Param("toUserId") Integer toUserId);

    Integer getSessionId(@Param("userId")Integer id,@Param("toUserId") Integer toUserId);


    void delSessionById(Integer id);
}




