package com.medical.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * 
 * @TableName sys_user
 */
@Data

public class SysUser implements Serializable {
    /**
     * 用户ID
     */
    @Id
    private Integer userId;

    /**
     * 用户名
     */
    private String userNickname;

    /**
     * 用户性别(女0 男1)
     */
    private Integer userGender;

    /**
     * 用户Token
     */
    private String userToken;

    /**
     * 用户头像
     */
    private String userAvatar;

    /**
     * 用户年龄
     */
    private String userAge;

    /**
     * 省份
     */
    private String userProvince;

    /**
     * 城市
     */
    private String userCity;

    /**
     * 地区
     */
    private String userCountry;

    /**
     * 联系电话
     */
    private String userPhone;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 身份称号
     */
    private String userAuthentication;


    /**
     * 评分
     */
    private  float userGrade;

    /**
     * 毕业信息
     */
    private  String userGraduate;

    @TableField(exist = false)
    private List<SysRole> role;

    private static final long serialVersionUID = 1L;
}