package com.medical.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * @TableName sys_role_menu
 */
@Data
@AllArgsConstructor
public class SysRoleMenu implements Serializable {
    /**
     * 角色菜单主键ID
     */
    private Long id;

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 菜单ID
     */
    private Long menuId;

    private static final long serialVersionUID = 1L;
}