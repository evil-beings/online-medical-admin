package com.medical.model;
/**
 * 手机破译实体
 */
public class DecodePhone {


    String encryptedData;
    String sessionId;
    String iv;

    @Override
    public String toString() {
        return "GetPhone{" +
                "encryptedData='" + encryptedData + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", iv='" + iv + '\'' +
                '}';
    }

    public String getEncryptedData() {
        return encryptedData;
    }

    public void setEncryptedData(String encryptedData) {
        this.encryptedData = encryptedData;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }
}
