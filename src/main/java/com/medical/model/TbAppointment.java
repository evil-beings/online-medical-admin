package com.medical.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 
 * @TableName tb_appointment
 */
@Data
public class TbAppointment implements Serializable {
    /**
     * id
     */
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    @TableField(exist = false)
    private SysUser sysUser;
    /**
     * 医生id
     */
    private Integer doctorId;

    /**
     * 预约时间
     */
    private String appointmentTime;

    /**
     * 是否完成
     */
    private int isComplete;

    /**
     * 评分
     */
    private int score;

    private static final long serialVersionUID = 1L;
}