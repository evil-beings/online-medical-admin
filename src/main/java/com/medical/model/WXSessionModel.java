package com.medical.model;

import lombok.Data;
/**
 * 微信小程序参数实体类
 */
@Data
public class WXSessionModel {

    private String session_key;
    private Integer expires_in;
    private String openid;
    private String phoneNumber;
    private Integer userId;
    
}
