package com.medical.model;

import lombok.Data;
/**
 * 手机号实体类
 */
@Data
public class Phone {

    private String phoneNumber;
    private String purePhoneNumber;
    private String countryCode;
    private Object watermark;


}
