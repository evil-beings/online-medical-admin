package com.medical.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author ziluolan
 * @description: 微信小程序秘钥实体类
 * @date 2023/5/11 16:55
 */
@Component
@Data
@ConfigurationProperties(prefix = "wx") //接收application.wx
public class WX {


    private String wxId;


    private String wxSecret;
}
