package com.medical.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 会话列表
 * @TableName session_list
 */
@Data
public class SessionList implements Serializable {
    /**
     * id
     */
    private Integer id;

    /**
     * 所属用户
     */
    private Integer userId;

    /**
     * 到用户
     */
    private Integer toUserId;

    /**
     * 会话名称
     */
    private String listName;



    /**
     * 未读消息数
     */
    private Integer unReadCount;

    /**
     * 用户头像
     */
    @TableField(exist = false)
    private String userAvatar;

    @TableField(exist = false)
    private String role;

    private static final long serialVersionUID = 1L;
}