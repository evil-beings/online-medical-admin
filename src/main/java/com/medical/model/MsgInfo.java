package com.medical.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 消息表
 * @TableName msg_info
 */
@Data
public class MsgInfo implements Serializable {
    /**
     * 消息id
     */
    private Integer id;

    /**
     * 消息发送者id
     */
    private Integer fromUserId;

    /**
     * 消息发送者名称
     */
    private String fromUserName;

    /**
     * 消息接收者id
     */
    private Integer toUserId;

    /**
     * 消息接收者名称
     */
    private String toUserName;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 消息发送时间
     */

    private Date createTime;

    /**
     * 是否已读（1 已读）
     */
    private Integer unReadFlag;

    private static final long serialVersionUID = 1L;
}