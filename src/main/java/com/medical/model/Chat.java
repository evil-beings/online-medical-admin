package com.medical.model;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Description TODO
 * @Author ziluolan
 * @Date 2023/5/13 14:27
 */
@Data
public class Chat {
    private Long id;

    private Long sendUserId;

    private Long receiveUserId;

    private LocalDateTime createTime;

    private String content;

}
