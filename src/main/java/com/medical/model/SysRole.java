package com.medical.model;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * 
 * @TableName sys_role
 */

@Data
public class SysRole implements Serializable {
    /**
     * 角色主键ID
     */
    private Long id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色权限字符串
     */
    private String code;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;
}