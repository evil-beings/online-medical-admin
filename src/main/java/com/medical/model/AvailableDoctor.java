package com.medical.model;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description TODO
 * @Author ziluolan
 * @Date 2023/6/9 19:00
 */
@Data
public class AvailableDoctor implements Serializable {

    private SysUser sysUser;
    private int count=0;
    private List<String> avaTime=new ArrayList<>();

    public void addAvaTime(String date) {
        avaTime.add(date);
    }
    public void addNum() {
        count++;
    }

}
