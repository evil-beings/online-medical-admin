package com.medical.common.globalException;

import com.medical.util.status.BaseResponse;
import lombok.Data;

/**
 * @author ziluolan
 * @description: 自定义异常result集
 * @date 2023/5/9 23:48
 */


@Data
public class MyException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    protected Integer errorCode;
    /**
     * 错误信息
     */
    protected String errorMsg;

    public MyException() {
        super();
    }

    public MyException(BaseResponse errorInfoInterface) {
        super(errorInfoInterface.getCode().toString());
        this.errorCode = errorInfoInterface.getCode();
        this.errorMsg = errorInfoInterface.getMsg();
    }

    public MyException(BaseResponse errorInfoInterface, Throwable cause) {
        super(errorInfoInterface.getCode().toString(), cause);
        this.errorCode = errorInfoInterface.getCode();
        this.errorMsg = errorInfoInterface.getMsg();
    }

    public MyException(String errorMsg) {
        super(errorMsg);
        this.errorMsg = errorMsg;
    }

}