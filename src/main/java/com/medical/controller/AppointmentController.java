package com.medical.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.medical.model.AvailableDoctor;
import com.medical.model.SysUser;
import com.medical.model.TbAppointment;
import com.medical.service.SysUserService;
import com.medical.service.TbAppointmentService;
import com.medical.util.status.BaseResponse;
import com.medical.util.status.CodeEnum;
import com.medical.util.status.ResponseData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description TODO
 * @Author ziluolan
 * @Date 2023/6/8 8:28
 */
@RestController
@RequestMapping("/wx")
public class AppointmentController {
    @Autowired
    TbAppointmentService appointmentService;
    @Autowired
    SysUserService sysUserService;



    /**
     * 通过时间查询可预约的医生
     */
    @GetMapping("/appointment/time/{time}")
    @Transactional
    @ApiOperation(value = "查询预约")
    public BaseResponse queryByTime(@PathVariable String time ) throws ParseException {


        List<SysUser> doctorList = sysUserService.getDoctorList();


        List<AvailableDoctor> availableDoctorList=new ArrayList<>();

        for (int i=0;i< doctorList.size();i++){
            AvailableDoctor availableDoctor=new AvailableDoctor();
            System.out.println(appointmentService.list(new QueryWrapper<TbAppointment>().eq("appointment_time",time+" "+"15:00").eq("doctor_id",doctorList.get(i).getUserId())));
            if(appointmentService.list(new QueryWrapper<TbAppointment>().eq("appointment_time",time+" "+"15:00").eq("doctor_id",doctorList.get(i).getUserId())).size()==0){
                String dateString = "15:00";
                availableDoctor.addAvaTime("15:00");
                availableDoctor.addNum();
                availableDoctor.setSysUser(doctorList.get(i));
            }
            if(appointmentService.list(new QueryWrapper<TbAppointment>().eq("appointment_time",time+" "+"16:00").eq("doctor_id",doctorList.get(i).getUserId())).size()==0){
                String dateString = "16:00";
                availableDoctor.addAvaTime("16:00");
                availableDoctor.addNum();
                availableDoctor.setSysUser(doctorList.get(i));
            }
            if(appointmentService.list(new QueryWrapper<TbAppointment>().eq("appointment_time",time+" "+"17:00").eq("doctor_id",doctorList.get(i).getUserId())).size()==0){
                String dateString = "17:00";
                availableDoctor.addAvaTime("17:00");
                availableDoctor.addNum();
                availableDoctor.setSysUser(doctorList.get(i));
            }
            availableDoctorList.add(availableDoctor);

        }




        return ResponseData.success(availableDoctorList);

    }

    /**
     * 用户查询我的预约
     */
    @GetMapping("/appointment/{uid}")
    @Transactional
    @ApiOperation(value = "查询预约")
    public BaseResponse query(@PathVariable Integer uid ) {

        List<TbAppointment> list = appointmentService.list(new QueryWrapper<TbAppointment>().eq("user_id", uid));

        for(TbAppointment appointment:list){
            SysUser one = sysUserService.getOne(new QueryWrapper<SysUser>().eq("user_id", appointment.getDoctorId()));
            appointment.setSysUser(one);

        }
        return ResponseData.success(list);

    }


    /**
     * 查询我的评分
     */
    @GetMapping("/appointment/score/{uid}")
    @Transactional
    @ApiOperation(value = "查询我的评分")
    public BaseResponse queryScore(@PathVariable Integer uid ) {

        List<TbAppointment> list = appointmentService.list(new QueryWrapper<TbAppointment>().eq("user_id", uid).eq("is_complete",1));

        for(TbAppointment appointment:list){
            SysUser one = sysUserService.getOne(new QueryWrapper<SysUser>().eq("user_id", appointment.getDoctorId()));
            appointment.setSysUser(one);

        }
        return ResponseData.success(list);

    }

    /**
     * 查询我的评分(医生)
     */
    @GetMapping("/appointment/doctor/score/{did}")
    @Transactional
    @ApiOperation(value = "查询我的评分")
    public BaseResponse queryScoreD(@PathVariable Integer did ) {

        List<TbAppointment> list = appointmentService.list(new QueryWrapper<TbAppointment>().eq("doctor_id", did).eq("is_complete",1));

        for(TbAppointment appointment:list){
            SysUser one = sysUserService.getOne(new QueryWrapper<SysUser>().eq("user_id", appointment.getDoctorId()));
            appointment.setSysUser(one);

        }
        return ResponseData.success(list);

    }



    /**
     * 新增预约
     */
    @PostMapping("/appointment/add")
    @Transactional
    @ApiOperation(value = "新增预约")
    public BaseResponse insert(@RequestBody TbAppointment appointment ) {

         appointmentService.save(appointment);
         return BaseResponse.out(CodeEnum.SUCCESS);

    }


    /**
     * 修改预约为完成
     */
    @PostMapping("/appointment/update/{id}")
    @Transactional
    @ApiOperation(value = "修改预约")
    public BaseResponse update(@PathVariable Integer id  ) {

        TbAppointment appointment=new TbAppointment();
        appointment.setId(id);
        appointment.setIsComplete(1);
        appointmentService.updateById(appointment);
        return BaseResponse.out(CodeEnum.SUCCESS);

    }


    /**
     * 评分
     */
    @PostMapping("/appointment/update/{id}/{score}")
    @Transactional
    @ApiOperation(value = "评分")
    public BaseResponse updateScore(@PathVariable Integer id,@PathVariable int score  ) {

        TbAppointment appointment=new TbAppointment();//新建评分对象
        appointment.setId(id);//设置id为前端获取的评分数据库id
        appointment.setScore(score);//设置分数为前端传回来的分数
        appointment.setIsComplete(1);//设置为已完成评分，防止修改时将此项修改
        appointmentService.updateById(appointment);//调用mybatis plus方法修改数据库字段
        return BaseResponse.out(CodeEnum.SUCCESS);//返回成功消息

    }



    /**
     * 取消预约
     */
    @PostMapping("/appointment/del/{id}")
    @Transactional
    @ApiOperation(value = "取消预约")
    public BaseResponse delete(@PathVariable Integer id ) {

        appointmentService.removeById(id);
        return BaseResponse.out(CodeEnum.SUCCESS);

    }

}
