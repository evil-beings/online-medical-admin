package com.medical.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.medical.model.SysMenu;
import com.medical.model.SysMenu;
import com.medical.service.SysMenuService;
import com.medical.util.status.BaseResponse;
import com.medical.util.status.CodeEnum;
import com.medical.util.status.ResponseData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
/**
 *@Description 菜单控制层
 *@Author ziluolan
 *@Date 2023/5/12 13:31
 */
@RestController
@RequestMapping("/wx")
public class SysMenuController {

@Autowired
    SysMenuService sysMenuService;


    /**
     * 获取菜单列表
     */
    @PostMapping("/menu/List")
    @Transactional
    @ApiOperation(value = "获取菜单列表")
    public BaseResponse getMenuList() {
        HashMap map = new HashMap<String,Object>();
        List<SysMenu> menuList = sysMenuService.list();
        map.put("menuList", menuList);
        return ResponseData.success(map);
    }


    /**
     * 通过id获取菜单列表
     */
    @PostMapping("/menu/List/{id}")
    @Transactional
    @ApiOperation(value = "通过id获取菜单列表")
    public BaseResponse getMenuListById(@PathVariable Integer id) {
       HashMap map = new HashMap<>();
        QueryWrapper<Integer> wrapper = new QueryWrapper<>();
        wrapper.eq("menu_id", id);
        List<SysMenu>menusList = sysMenuService.list();
        map.put("menusList", menusList);

        return ResponseData.success(map);
    }



    /**
     * 新增菜单
     */
    @PostMapping("/menu/add")
    @Transactional
    @ApiOperation(value = "新增角色")
    public BaseResponse insertMenu(@RequestBody SysMenu sysMenu) {

        boolean save = sysMenuService.save(sysMenu);
        if (save) return BaseResponse.out(CodeEnum.SUCCESS);
        return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);
    }

    /**
     * 修改菜单
     */
    @PostMapping("/menu/update")
    @Transactional
    @ApiOperation(value = "修改菜单")
    public BaseResponse updateMenu(@RequestBody SysMenu sysMenu, Integer id) {

        boolean save = sysMenuService.update(sysMenu, new QueryWrapper<SysMenu>().eq("id", id));
        if (save) return BaseResponse.out(CodeEnum.SUCCESS);
        return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);
    }

    /**
     * 删除菜单
     */
    @PostMapping("/menu/delete")
    @Transactional
    @ApiOperation(value = "删除菜单")
    public BaseResponse deleteMenu(Integer id) {

        boolean save = sysMenuService.removeById(id);
        if (save) return BaseResponse.out(CodeEnum.SUCCESS);
        return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);
    }

}
