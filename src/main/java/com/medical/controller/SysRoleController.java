package com.medical.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.medical.mapper.SysRoleMenuMapper;
import com.medical.mapper.SysUserRoleMapper;
import com.medical.model.SysMenu;
import com.medical.model.SysRole;
import com.medical.model.SysUser;
import com.medical.service.SysMenuService;
import com.medical.service.SysRoleMenuService;
import com.medical.service.SysRoleService;
import com.medical.service.SysUserRoleService;
import com.medical.util.status.BaseResponse;
import com.medical.util.status.CodeEnum;
import com.medical.util.status.ResponseData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @ClassName SysRoleController
 * @Description 角色控制层
 * @Author ziluolan
 * @Date 2023/5/11 8:52
 * @Version 1.0
 */
@RestController
@RequestMapping("/wx")
public class SysRoleController {
    @Autowired
    SysRoleService sysRoleService;
    @Resource
    SysMenuService sysMenuService;
    @Autowired
    SysRoleMenuService sysRoleMenuService;



    /**
     * 获取角色列表
     */
    @PostMapping("/role/List")
    @Transactional
    @ApiOperation(value = "获取角色列表")
    public BaseResponse getRoleList() {
        HashMap<String, Object> map = new HashMap();
        List<SysRole> rolesList = sysRoleService.list();
        map.put("rolesList", rolesList);

        return ResponseData.success(map);
    }

    /**
     * 通过id获取角色列表
     */
    @PostMapping("/role/List/{id}")
    @Transactional
    @ApiOperation(value = "通过id获取角色列表")
    public BaseResponse getRoleListById(@PathVariable Integer id) {
      HashMap  map = new HashMap();
       QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("role_id", id);
        List<SysRole> rolesList = sysRoleService.list();
        map.put("rolesList", rolesList);

        return ResponseData.success(map);
    }

    /**
     * 通过对象查询获取角色列表
     */
    @PostMapping("/role/List/query")
    @Transactional
    @ApiOperation(value = "通过对象查询获取角色列表")
    public BaseResponse getRoleList(@RequestBody SysRole sysRole) {
      HashMap  map = new HashMap();
       QueryWrapper wrapper = new QueryWrapper<SysRole>(sysRole);
        List<SysRole> rolesList = sysRoleService.list(wrapper);
        int count = sysRoleService.count(wrapper);
        map.put("rolesList", rolesList);
        map.put("count", count);
        return ResponseData.success(map);
    }

    /**
     * 新增角色
     */
    @PostMapping("/role/add")
    @Transactional
    @ApiOperation(value = "新增角色")
    public BaseResponse insertRole(@RequestBody SysRole sysRole) {

        boolean save = sysRoleService.save(sysRole);
        if (save) return BaseResponse.out(CodeEnum.SUCCESS);
        return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);
    }

    /**
     * 修改角色
     */
    @PostMapping("/role/update")
    @Transactional
    @ApiOperation(value = "修改角色")
    public BaseResponse updateRole(@RequestBody SysRole sysRole, Integer id) {

        boolean save = sysRoleService.update(sysRole, new QueryWrapper<SysRole>().eq("id", id));
        if (save) return BaseResponse.out(CodeEnum.SUCCESS);
        return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);
    }

    /**
     * 删除角色
     */
    @PostMapping("/role/delete")
    @Transactional
    @ApiOperation(value = "修改角色")
    public BaseResponse deleteRole(Integer id) {

        boolean save = sysRoleService.removeById(id);
        if (save) return BaseResponse.out(CodeEnum.SUCCESS);
        return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);
    }

    /**
     * 通过角色id查询菜单
     */
    @PostMapping("/role/menu/queryById")
    @Transactional
    @ApiOperation(value = "通过角色id查询菜单")
    public BaseResponse getRoleMenuList(Integer id) {

        List<SysMenu> roleMenuList = sysMenuService.getRoleMenuList(id);

        return ResponseData.success(roleMenuList);
    }
    /**
     * 修改角色对应的菜单通过角色id
     */
    @PostMapping("/role/menu/update")
    @Transactional
    @ApiOperation(value = "修改角色对应的菜单")
    public BaseResponse updateRoleMenuList(@RequestBody List<SysMenu> sysMenus, Long id) {

        int update = sysRoleMenuService.updateRoleMenu(sysMenus, id);
        if(update>0) return BaseResponse.out(CodeEnum.SUCCESS);
        return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);
    }
}
