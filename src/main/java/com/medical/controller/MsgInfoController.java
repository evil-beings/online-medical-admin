package com.medical.controller;



import com.medical.mapper.MsgInfoMapper;
import com.medical.mapper.SessionListMapper;
import com.medical.model.MsgInfo;
import com.medical.model.SessionList;
import com.medical.util.status.BaseResponse;
import com.medical.util.status.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/wx")
public class MsgInfoController {

    @Resource
    private MsgInfoMapper msgInfoMapper;

    @Resource
    private SessionListMapper sessionListMapper;

    // 消息列表
    @PostMapping("/msgList")
    public BaseResponse msgList(@RequestParam Integer sessionId){
        SessionList sessionList = sessionListMapper.selectById(sessionId);
        if(sessionList == null){
            return ResponseData.success();
        }
        Integer fromUserId = sessionList.getUserId();
        Integer toUserId = sessionList.getToUserId();
        List<MsgInfo> msgInfoList = msgInfoMapper.selectMsgList(fromUserId,toUserId);
        // 更新消息已读
        msgInfoMapper.msgRead(fromUserId, toUserId);
        // 更新会话里面的未读消息
        sessionListMapper.delUnReadCount(toUserId,fromUserId);
        return ResponseData.success(msgInfoList);
    }


}