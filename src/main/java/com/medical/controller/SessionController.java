package com.medical.controller;



import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.medical.mapper.MsgInfoMapper;
import com.medical.mapper.SessionListMapper;
import com.medical.mapper.SysUserMapper;
import com.medical.model.MsgInfo;
import com.medical.model.SessionList;
import com.medical.model.SysRole;
import com.medical.model.SysUser;
import com.medical.service.SysRoleService;
import com.medical.util.status.BaseResponse;
import com.medical.util.status.CodeEnum;
import com.medical.util.status.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/wx")
public class SessionController {

    @Resource
    private SysUserMapper userMapper;

    @Resource
    private SessionListMapper seesionListMapper;
    @Resource
    MsgInfoMapper msgInfoMapper;
    @Autowired
    SysRoleService sysRoleService;

    /**
     * 已建立会话
     */
    @PostMapping("/sessionList/already")
    public BaseResponse sessionListAlready(@RequestParam Integer id){
        List<SessionList> sessionLists = seesionListMapper.selectList(new QueryWrapper<SessionList>().eq("to_user_id",id));
        for(SessionList session: sessionLists){
            SysUser sysUser = userMapper.selectOne(new QueryWrapper<SysUser>().eq("user_id", session.getUserId()));
            List<SysRole> roleList = sysRoleService.list(new QueryWrapper<SysRole>().inSql("id", "select role_id from sys_user_role where user_id=" + session.getUserId()));
            if(roleList.stream().anyMatch(f -> f.getCode().contains("common"))){
                session.setRole("普通用户");
            }
            if(roleList.stream().anyMatch(f -> f.getCode().contains("doctor"))){
                session.setRole("医生");
            }
            if(roleList.stream().anyMatch(f -> f.getCode().contains("admin"))){
                session.setRole("超级管理员");
            }


            session.setUserAvatar(sysUser.getUserAvatar());
        }

        return  ResponseData.success(sessionLists);
    }

    // 可建立会话的用户
    @GetMapping("/sessionList/not")
    public BaseResponse sessionListNot(@RequestParam Integer id){
        List<Integer> list = seesionListMapper.selectUserIdByUserId(id);
        list.add(id);
        List<SysUser> cloudList = userMapper.getCloudList(list);
        return ResponseData.success(cloudList);
    }

    // 创建会话
    @PostMapping("/createSession")
    public BaseResponse createSession(@RequestParam Integer id,@RequestParam String userNickname,@RequestParam Integer toUserId,@RequestParam String toUsername){
        System.out.println("id:"+id+"touserid:"+toUserId);
        SessionList sessionList = new SessionList();

        Integer con = seesionListMapper.selectIdByUser(id, toUserId);

        // 判断我和对方建立会话没有？ 没有也要建立
        if(con==null|| con <= 0){
            sessionList.setUserId(id);
            sessionList.setUnReadCount(0);

            sessionList.setListName(userNickname);
            sessionList.setToUserId(toUserId);
            seesionListMapper.insert(sessionList);
        }


        // 判断对方和我建立会话没有？ 没有也要建立
        Integer integer = seesionListMapper.selectIdByUser(toUserId, id);
        System.out.println("integer:"+integer);

        if (integer == null || integer <= 0){
            sessionList.setUserId(toUserId);
            sessionList.setToUserId(id);
            sessionList.setListName(toUsername);
            sessionList.setUnReadCount(0);
            seesionListMapper.insert(sessionList);
        }
        Integer sessionId = seesionListMapper.getSessionId(id, toUserId);
        System.out.println("sessionId:"+sessionId);
        SessionList sessionList1=new SessionList();
        sessionList1.setId(sessionId);

        return ResponseData.success(sessionList1);
    }

    // 删除会话
    @PostMapping("/delSession")
    public BaseResponse delSession(@RequestParam Integer sessionId,@RequestParam Integer userId,@RequestParam Integer toUserId){
        System.out.println(sessionId);
        seesionListMapper.deleteById(sessionId);
        List<MsgInfo> msgInfos = msgInfoMapper.selectMsgList(userId, toUserId);
        for(MsgInfo msgInfo:msgInfos){
            msgInfoMapper.delete(new QueryWrapper<>(msgInfo) );
        }

        return ResponseData.out(CodeEnum.SUCCESS);
    }





}