package com.medical.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.medical.mapper.MsgInfoMapper;
import com.medical.mapper.SessionListMapper;
import com.medical.mapper.SysUserMapper;
import com.medical.model.SysMenu;
import com.medical.model.SysRole;
import com.medical.model.SysUser;
import com.medical.model.WXSessionModel;
import com.medical.service.SysMenuService;
import com.medical.service.SysRoleService;
import com.medical.service.SysUserRoleService;
import com.medical.service.SysUserService;
import com.medical.util.CheckMessageUtil;
import com.medical.util.status.BaseResponse;
import com.medical.util.status.CodeEnum;
import com.medical.util.status.ResponseData;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
/**
 *@ClassName UserController
 *@Description 用户控制层
 *@Author ziluolan
 *@Date 2023/5/10 21:44
 *@Version 1.0
 */
@RestController
@RequestMapping("/wx")
public class SysUserController {

    @Autowired
    SysUserService sysUserService;
    @Autowired
    SysRoleService sysRoleService;
    @Autowired
    SysUserRoleService sysUserRoleService;
    @Resource
    SysUserMapper sysUserMapper;
    @Resource
    SessionListMapper sessionListMapper;
    @Resource
    MsgInfoMapper msgInfoMapper;


    /**
     * 更改用户资料
     */
    @PostMapping("/user/change")
    @Transactional
    @ApiOperation(value = "更改用户资料")
    public BaseResponse changeUserMessage(@RequestBody SysUser userForm,HttpServletRequest request) {

        WXSessionModel wxSessionModel = (WXSessionModel) request.getSession().getAttribute("user");
       QueryWrapper<SysUser> wrapper= new QueryWrapper();
        wrapper.eq("user_id", wxSessionModel.getUserId());
        userForm.setUserGrade(sysUserMapper.selectOne(wrapper).getUserGrade());
        boolean update = sysUserService.update(userForm,wrapper );
        if(!update){
            return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);
        }
        return BaseResponse.out(CodeEnum.SUCCESS);
    }

    /**
     * 获取用户用户菜单
     */
    @PostMapping("/user/menu")
    @Transactional
    @ApiOperation(value = "获取用户用户菜单")
    public BaseResponse getUserMenu(HttpServletRequest request) {
        WXSessionModel wxSessionModel = (WXSessionModel) request.getSession().getAttribute("user");
        SysUser sysUser=new SysUser();
        sysUser.setUserId(wxSessionModel.getUserId());
        List<SysMenu> menus = sysUserService.getMenu(sysUser);

        return ResponseData.success(menus);
    }


    /**
     * 通过id获取用户列表
     */
    @PostMapping("/user/query")
    @Transactional
    @ApiOperation(value = "通过id获取用户列表")
    public BaseResponse getuserListById( @RequestParam Integer id) {
        HashMap<String,Object> map=new HashMap();
        QueryWrapper wrapper=new QueryWrapper();
        wrapper.eq("user_id",id);
       SysUser user = sysUserService.getOne(wrapper);
        map.put("user",user);

        return ResponseData.success(map);
    }

    /**
     * 通过对象查询获取用户列表
     */
    @PostMapping("/user/List/query")
    @Transactional
    @ApiOperation(value = "通过对象查询获取用户列表")
    public BaseResponse getUserList( @RequestParam String userNickname) {


        HashMap<String,Object> map=new HashMap();

        if(userNickname==null||userNickname==""){
            List<SysUser> usersList = sysUserService.list();
            int count = sysUserService.count();
            map.put("usersList",usersList);
            map.put("count",count);
        }else {

            QueryWrapper wrapper=new QueryWrapper<SysUser>().like("user_nickname",userNickname);
            List<SysUser> usersList = sysUserService.list(wrapper);
            int count = sysUserService.count(wrapper);
            map.put("usersList",usersList);
            map.put("count",count);

        }

        return ResponseData.success(map);
    }

    /**
     * 获取用户是医生的列表
     */
    @PostMapping("/user/doctorList")
    @Transactional
    @ApiOperation(value = "获取用户是医生的列表")
    public BaseResponse getDoctorList() {
        HashMap<String,Object> map=new HashMap();
        List<SysUser> usersList = sysUserService.getDoctorList();
        int count=0;
        for(SysUser sysUser:usersList){
            count++;
        }

        map.put("UsersList",usersList);
        map.put("count",count);

        return ResponseData.success(map);
    }

    /**
     * 获取评分高的5位医生的列表
     */
    @PostMapping("/user/list/grade")
    @Transactional
    @ApiOperation(value = "获取用户是医生的列表")
    public BaseResponse getDoctorGradeList() {
        HashMap<String,Object> map=new HashMap();
        List<SysUser> usersList = sysUserMapper.grade();
        int count=0;
        for(SysUser sysUser:usersList){
            count++;
        }

        map.put("usersList",usersList);
        map.put("count",count);

        return ResponseData.success(map);
    }


    /**
     * 新增用户
     */
    @PostMapping("/user/add")
    @Transactional
    @ApiOperation(value = "新增用户")
    public BaseResponse insertUser(@RequestBody SysUser sysUser ) {

        boolean save = sysUserService.save(sysUser);
        if(save) return BaseResponse.out(CodeEnum.SUCCESS);
        return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);
    }

    /**
     * 修改用户
     */
    @PostMapping("/user/update")
    @Transactional
    @ApiOperation(value = "修改用户")
    public BaseResponse updateUser(@RequestBody SysUser sysUser,Integer id) {

        boolean save = sysUserService.update(sysUser,new QueryWrapper<SysUser>().eq("user_id",id));
        if(save) return BaseResponse.out(CodeEnum.SUCCESS);
        return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);
    }

    /**
     * 删除用户
     */
    @PostMapping("/user/delete")
    @Transactional
    @ApiOperation(value = "修改用户")
    public BaseResponse deleteUser(Integer id) {

        boolean save = sysUserService.remove(new QueryWrapper<SysUser>().eq("user_id",id));
        sessionListMapper.delSessionById(id);
        msgInfoMapper.delMsgById(id);

        if(save) return BaseResponse.out(CodeEnum.SUCCESS);
        return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);
    }


    /**
     * 通过用户id查询角色
     */
    @PostMapping("/user/role/queryById")
    @Transactional
    @ApiOperation(value = "通过用户id查询角色")
    public BaseResponse getUSerRoleList(Integer id) {

        List<SysRole> userRoleList = sysRoleService.getUserRoleList(id);

        return ResponseData.success(userRoleList);
    }
    /**
     * 修改用户对应的角色通过用户id
     */
    @PostMapping("/user/role/update")
    @Transactional
    @ApiOperation(value = "修改用户对应的角色通过用户id")
    public BaseResponse updateUserRoleList(@RequestBody List<SysRole> sysRoles, Long id) {


        int update = sysUserRoleService.updateUserRole(sysRoles, id);
        if(update>0) return BaseResponse.out(CodeEnum.SUCCESS);
        return BaseResponse.out(CodeEnum.INTERNAL_SERVER_ERROR);

    }

}