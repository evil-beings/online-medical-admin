package com.medical.service;

import com.medical.model.MsgInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author ziluolan
* @description 针对表【msg_info(消息表)】的数据库操作Service
* @createDate 2023-05-13 21:40:46
*/
public interface MsgInfoService extends IService<MsgInfo> {

}
