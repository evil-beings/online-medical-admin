package com.medical.service;

import com.medical.model.SysRole;
import com.medical.model.SysUser;
import com.medical.model.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author ziluolan
* @description 针对表【sys_user_role】的数据库操作Service
* @createDate 2023-05-09 21:34:22
*/
public interface SysUserRoleService extends IService<SysUserRole> {

    void setDefaultRole(SysUser one);

    int updateUserRole(List<SysRole> sysRoles, Long id);
}
