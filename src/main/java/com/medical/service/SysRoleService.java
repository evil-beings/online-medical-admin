package com.medical.service;

import com.medical.model.SysMenu;
import com.medical.model.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.medical.model.SysUser;

import java.util.List;

/**
* @author ziluolan
* @description 针对表【sys_role】的数据库操作Service
* @createDate 2023-05-09 21:33:58
*/
public interface SysRoleService extends IService<SysRole> {


    List<SysRole> getUserRoleList(Integer id);
}
