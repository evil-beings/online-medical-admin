package com.medical.service;

import com.medical.model.SysMenu;
import com.medical.model.SysRole;
import com.medical.model.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author ziluolan
* @description 针对表【sys_user】的数据库操作Service
* @createDate 2023-05-09 21:35:41
*/
public interface SysUserService extends IService<SysUser> {

    List<SysMenu> getMenu(SysUser sysUser);
    List<SysUser> getDoctorList();

}
