package com.medical.service;

import com.medical.model.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author ziluolan
* @description 针对表【sys_menu】的数据库操作Service
* @createDate 2023-05-11 11:25:48
*/
public interface SysMenuService extends IService<SysMenu> {
    List<SysMenu> getRoleMenuList(Integer id);
}
