package com.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.medical.model.TbAppointment;
import com.medical.service.TbAppointmentService;
import com.medical.mapper.TbAppointmentMapper;
import org.springframework.stereotype.Service;

/**
* @author ziluolan
* @description 针对表【tb_appointment】的数据库操作Service实现
* @createDate 2023-06-10 11:00:28
*/
@Service
public class TbAppointmentServiceImpl extends ServiceImpl<TbAppointmentMapper, TbAppointment>
    implements TbAppointmentService{

}




