package com.medical.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.medical.mapper.SysMenuMapper;
import com.medical.mapper.SysRoleMenuMapper;
import com.medical.model.SysMenu;
import com.medical.model.SysRole;
import com.medical.model.SysUser;
import com.medical.model.SysUserRole;
import com.medical.service.SysRoleService;
import com.medical.mapper.SysRoleMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author ziluolan
* @description 针对表【sys_role】的数据库操作Service实现
* @createDate 2023-05-09 21:33:58
*/
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole>
    implements SysRoleService{
    @Resource
    SysRoleMapper sysRoleMapper;
    /**
     * 查询对应用户的角色列表
     */
    @Override
    public List<SysRole> getUserRoleList(Integer id) {
        return sysRoleMapper.selectList(new QueryWrapper<SysRole>().inSql("id", "select role_id from sys_user_role where user_id=" + id));

    }
}




