package com.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.medical.model.MsgInfo;
import com.medical.service.MsgInfoService;
import com.medical.mapper.MsgInfoMapper;
import org.springframework.stereotype.Service;

/**
* @author ziluolan
* @description 针对表【msg_info(消息表)】的数据库操作Service实现
* @createDate 2023-05-13 21:40:46
*/
@Service
public class MsgInfoServiceImpl extends ServiceImpl<MsgInfoMapper, MsgInfo>
    implements MsgInfoService{

}




