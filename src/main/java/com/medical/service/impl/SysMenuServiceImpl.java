package com.medical.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.medical.model.SysMenu;
import com.medical.service.SysMenuService;
import com.medical.mapper.SysMenuMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author ziluolan
* @description 针对表【sys_menu】的数据库操作Service实现
* @createDate 2023-05-11 11:25:48
*/
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu>
    implements SysMenuService{
    @Resource
    SysMenuMapper sysMenuMapper;
    /**
     * 查询对应角色的菜单列表
     */
    @Override
    public List<SysMenu> getRoleMenuList(Integer id) {
        return sysMenuMapper.selectList(new QueryWrapper<SysMenu>().inSql("id", "select menu_id from sys_role_menu where role_id=" + id));

    }
}




