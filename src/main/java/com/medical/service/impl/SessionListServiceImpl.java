package com.medical.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.medical.model.SessionList;
import com.medical.service.SessionListService;
import com.medical.mapper.SessionListMapper;
import org.springframework.stereotype.Service;

/**
* @author ziluolan
* @description 针对表【session_list(会话列表)】的数据库操作Service实现
* @createDate 2023-05-13 21:40:46
*/
@Service
public class SessionListServiceImpl extends ServiceImpl<SessionListMapper, SessionList>
    implements SessionListService{

}




