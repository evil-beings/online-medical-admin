package com.medical.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.medical.mapper.SysMenuMapper;
import com.medical.model.SysMenu;
import com.medical.model.SysRoleMenu;
import com.medical.service.SysRoleMenuService;
import com.medical.mapper.SysRoleMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author ziluolan
* @description 针对表【sys_role_menu】的数据库操作Service实现
* @createDate 2023-05-11 11:25:58
*/
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu>
    implements SysRoleMenuService{
    /**
     * 修改角色对应的菜单通过角色id
     */
@Resource
SysRoleMenuMapper sysRoleMenuMapper;
    @Override
    public int updateRoleMenu(List<SysMenu> sysMenus, Long id) {
        int update=0;
        List<SysRoleMenu> roleMenus = sysRoleMenuMapper.selectList(new QueryWrapper<SysRoleMenu>().eq("role_id", id));
        for (SysRoleMenu roleMenu : roleMenus) {
            sysRoleMenuMapper.delete(new QueryWrapper<SysRoleMenu>().eq("role_id", roleMenu.getRoleId()));
        }
        for (SysMenu tem : sysMenus) {
            int insert = sysRoleMenuMapper.insert(new SysRoleMenu(null, id, tem.getId()));
            update+=insert;

        }
        return update;



    }
}




