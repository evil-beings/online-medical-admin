package com.medical.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.medical.mapper.SysMenuMapper;
import com.medical.mapper.SysRoleMapper;
import com.medical.mapper.SysUserRoleMapper;
import com.medical.model.SysMenu;
import com.medical.model.SysRole;
import com.medical.model.SysUser;
import com.medical.model.SysUserRole;
import com.medical.service.SysMenuService;
import com.medical.service.SysRoleService;
import com.medical.service.SysUserRoleService;
import com.medical.service.SysUserService;
import com.medical.mapper.SysUserMapper;
import com.medical.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author ziluolan
 * @description 针对表【sys_user】的数据库操作Service实现
 * @createDate 2023-05-09 21:35:41
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser>
        implements SysUserService {
    @Resource
    SysRoleMapper sysRoleMapper;
    @Resource
    SysMenuMapper sysMenuMapper;
    @Resource
    SysUserMapper sysUserMapper;

    /**
     * 查询登录用户的菜单
     */
    @Override
    public List<SysMenu> getMenu(SysUser sysUser) {
        List<SysMenu> sysMenuList=new ArrayList<>();

        // 获取角色
        List<SysRole> roleList = sysRoleMapper.selectList(new QueryWrapper<SysRole>().inSql("id", "select role_id from sys_user_role where user_id=" + sysUser.getUserId()));

        if (roleList.size() > 0) {
            List<SysMenu> sysMenus = new ArrayList<>();
            for(int i=0;i<roleList.size();i++){
                List<SysMenu> menus = sysMenuMapper.selectList(new QueryWrapper<SysMenu>().inSql("id", "select menu_id from sys_role_menu where role_id=" + roleList.get(i).getId()));

                sysMenus.addAll(menus);


               sysMenuList = sysMenus.stream().distinct().collect(Collectors.toList());


            }



        }


        return sysMenuList;
    }

    @Override
    public List<SysUser> getDoctorList() {
        return sysUserMapper.selectList(new QueryWrapper<SysUser>().inSql("user_id", "select user_id from sys_user_role where role_id=" + "2"));
    }


}




