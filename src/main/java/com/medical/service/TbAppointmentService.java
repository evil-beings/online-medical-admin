package com.medical.service;

import com.medical.model.TbAppointment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author ziluolan
* @description 针对表【tb_appointment】的数据库操作Service
* @createDate 2023-06-10 11:00:28
*/
public interface TbAppointmentService extends IService<TbAppointment> {

}
