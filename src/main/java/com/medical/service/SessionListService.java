package com.medical.service;

import com.medical.model.SessionList;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author ziluolan
* @description 针对表【session_list(会话列表)】的数据库操作Service
* @createDate 2023-05-13 21:40:46
*/
public interface SessionListService extends IService<SessionList> {

}
