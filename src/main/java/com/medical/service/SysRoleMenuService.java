package com.medical.service;

import com.medical.model.SysMenu;
import com.medical.model.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author ziluolan
* @description 针对表【sys_role_menu】的数据库操作Service
* @createDate 2023-05-11 11:25:58
*/
public interface SysRoleMenuService extends IService<SysRoleMenu> {


    int updateRoleMenu(List<SysMenu> sysMenus, Long id);
}
