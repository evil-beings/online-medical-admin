package com.medical;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;
@MapperScan("com.medical.mapper")
@SpringBootApplication
public class MedicalApplication {

    public static void main(String[] args) throws Exception {
        Environment env = SpringApplication.run(MedicalApplication.class, args).getEnvironment();
        System.out.println("(♥◠‿◠)ﾉﾞ  服务启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "Application started successfully, access URLs: \n"+
                "Local service:     http://localhost:" + env.getProperty("server.port")+"\n"+
                "External service:  http://" + InetAddress.getLocalHost().getHostAddress() + ":" + env.getProperty("server.port")+"\n"+
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }

}
