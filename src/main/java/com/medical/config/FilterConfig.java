package com.medical.config;


import com.medical.common.handlerInterceptor.MyInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * 拦截器配置
 */

@Configuration
public class FilterConfig implements WebMvcConfigurer {
    /**
     * 注入自定义拦截器
     */

    @Bean
    public MyInterceptor getMyInterceptor() {
        return new MyInterceptor();
    }

    /**
     * 拦截所有请求 exclude login
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry
                .addInterceptor(getMyInterceptor())
                .addPathPatterns("/wx/**")
                .excludePathPatterns("/wx/login")
                .excludePathPatterns("/wx/api/**")
                .excludePathPatterns("/wx/user/doctorList")
                .excludePathPatterns("/user/list/grade");


//        registry.addInterceptor(MyInterceptor()).addPathPatterns("/api/**").excludePathPatterns("/api/login");

    }


//    @Bean
//    public FilterRegistrationBean jwtFilter() {
//        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
//        registrationBean.setFilter(new JwtFilter());
//        //添加需要拦截的url
//        List<String> urlPatterns = Lists.newArrayList();
//        urlPatterns.add("/api/");
//
//        registrationBean.addUrlPatterns(urlPatterns.toArray(new String[urlPatterns.size()]));
//        return registrationBean;
//    }


}
