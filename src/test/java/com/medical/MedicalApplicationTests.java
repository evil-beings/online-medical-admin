package com.medical;

import com.medical.service.SysUserService;
import com.medical.service.impl.SysUserServiceImpl;


import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MedicalApplicationTests {
    @Autowired
    private SysUserServiceImpl sysUserService;

    @Test
    void start() {
        System.out.println(sysUserService.list());
    }

}
