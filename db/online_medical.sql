/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80033
 Source Host           : localhost:3306
 Source Schema         : online_medical

 Target Server Type    : MySQL
 Target Server Version : 80033
 File Encoding         : 65001

 Date: 10/10/2023 18:02:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for msg_info
-- ----------------------------
DROP TABLE IF EXISTS `msg_info`;
CREATE TABLE `msg_info`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '消息id',
  `from_user_id` int NOT NULL COMMENT '消息发送者id',
  `from_user_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '消息发送者名称',
  `to_user_id` int NOT NULL COMMENT '消息接收者id',
  `to_user_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '消息接收者名称',
  `content` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '消息内容',
  `create_time` datetime NOT NULL COMMENT '消息发送时间',
  `un_read_flag` int NOT NULL COMMENT '是否已读（1 已读）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 286 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '消息表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of msg_info
-- ----------------------------
INSERT INTO `msg_info` VALUES (273, 1, '我', 23, '他', '11111', '2023-05-13 21:20:08', 1);
INSERT INTO `msg_info` VALUES (274, 23, '他', 1, '我', '123', '2023-05-13 21:20:52', 1);
INSERT INTO `msg_info` VALUES (275, 1, '我', 23, '他', '自己动手\n', '2023-05-13 21:21:00', 1);
INSERT INTO `msg_info` VALUES (276, 23, '他', 1, '我', '11', '2023-05-13 21:28:55', 1);
INSERT INTO `msg_info` VALUES (277, 1, '管理员', 23, '管理员', 'sdsad', '2023-05-14 21:19:57', 1);
INSERT INTO `msg_info` VALUES (278, 1, '管理员', 23, '管理员', '你好', '2023-05-14 21:20:40', 1);
INSERT INTO `msg_info` VALUES (279, 1, '管理员', 23, '管理员', 'osdfhshfdsho', '2023-05-14 21:22:21', 1);
INSERT INTO `msg_info` VALUES (280, 1, '管理员', 23, '管理员', 'zxczCxc', '2023-05-14 21:22:29', 1);
INSERT INTO `msg_info` VALUES (281, 1, '管理员', 23, '管理员', '21dv4xd45c6v456v4c465', '2023-05-14 21:27:51', 1);
INSERT INTO `msg_info` VALUES (282, 1, '管理员', 23, '管理员', '111', '2023-05-14 21:32:02', 1);
INSERT INTO `msg_info` VALUES (283, 1, '管理员', 23, '管理员', '11111', '2023-05-14 21:32:09', 1);
INSERT INTO `msg_info` VALUES (284, 1, '管理员', 23, '管理员', '1111', '2023-05-14 21:34:29', 0);
INSERT INTO `msg_info` VALUES (285, 1, '管理员', 23, '管理员', 'xxx', '2023-05-14 21:35:13', 0);
INSERT INTO `msg_info` VALUES (286, 1, '管理员', 23, '管理员', '垃圾', '2023-05-14 21:59:28', 0);

-- ----------------------------
-- Table structure for session_list
-- ----------------------------
DROP TABLE IF EXISTS `session_list`;
CREATE TABLE `session_list`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int NOT NULL COMMENT '所属用户',
  `to_user_id` int NOT NULL COMMENT '到用户',
  `list_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '会话名称',
  `un_read_count` int NOT NULL COMMENT '未读消息数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 126 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '会话列表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of session_list
-- ----------------------------
INSERT INTO `session_list` VALUES (52, 21, 20, '123', 0);
INSERT INTO `session_list` VALUES (54, 20, 21, 'qqq', 0);
INSERT INTO `session_list` VALUES (125, 1, 23, '管理员', 0);
INSERT INTO `session_list` VALUES (126, 23, 1, '测试', 10);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单主键ID',
  `name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '父菜单ID',
  `order_num` int NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `menu_type` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `perms` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '权限标识',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '个人信息', 'icon-mingpiansheji', 0, 1, '/pages/system/system', 'M', '', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '账号安全中心', 'icon-mingpiansheji', 0, 2, '', 'M', '', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '业务管理目录');
INSERT INTO `sys_menu` VALUES (3, '收藏', 'icon-mingpiansheji', 1, 1, '/pages/user/user', 'C', 'system:user:list', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (4, '打印', 'icon-mingpiansheji', 1, 2, '/pages/role/role', 'C', 'system:role:list', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (5, '意见反馈', 'icon-mingpiansheji', 3, 2, '/pages/menu/menu', 'F', 'system:user:add', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '添加用户按钮');
INSERT INTO `sys_menu` VALUES (6, '程序管理', 'icon-mingpiansheji', 3, 3, '/pages/menu/menu', 'F', 'system:user:edit', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '修改用户按钮');
INSERT INTO `sys_menu` VALUES (7, '用户管理', 'icon-mingpiansheji', 3, 4, '/pages/menu/menu', 'F', 'system:user:delete', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '删除用户按钮');
INSERT INTO `sys_menu` VALUES (8, '角色管理', 'icon-mingpiansheji', 3, 5, '/pages/menu/menu', 'F', 'system:user:role', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '分配角色按钮');
INSERT INTO `sys_menu` VALUES (9, '菜单管理', 'icon-mingpiansheji', 3, 6, '/pages/menu/menu', 'F', 'system:user:resetPwd', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '重置密码按钮');
INSERT INTO `sys_menu` VALUES (10, '反馈信息管理', 'icon-mingpiansheji', 4, 2, '/pages/menu/menu', 'F', 'system:role:add', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '添加用户按钮');
INSERT INTO `sys_menu` VALUES (11, '我的评分', 'icon-mingpiansheji', 4, 3, '/pages/menu/menu', 'F', 'system:role:edit', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '修改用户按钮');
INSERT INTO `sys_menu` VALUES (12, '发表论文', 'icon-mingpiansheji', 4, 4, '/pages/menu/menu', 'F', 'system:role:delete', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '删除用户按钮');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色主键ID',
  `name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `code` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '角色权限字符串',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '拥有系统最高权限');
INSERT INTO `sys_role` VALUES (2, '医生', 'doctor', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '医护人员');
INSERT INTO `sys_role` VALUES (3, '普通用户', 'common', '2023-05-08 08:47:52', '2023-05-08 08:47:52', '普通角色');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色菜单主键ID',
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 249 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (8, 3, 1);
INSERT INTO `sys_role_menu` VALUES (239, 3, 2);
INSERT INTO `sys_role_menu` VALUES (240, 3, 3);
INSERT INTO `sys_role_menu` VALUES (241, 3, 4);
INSERT INTO `sys_role_menu` VALUES (242, 3, 5);
INSERT INTO `sys_role_menu` VALUES (243, 1, 6);
INSERT INTO `sys_role_menu` VALUES (244, 1, 7);
INSERT INTO `sys_role_menu` VALUES (245, 1, 8);
INSERT INTO `sys_role_menu` VALUES (246, 1, 9);
INSERT INTO `sys_role_menu` VALUES (247, 1, 10);
INSERT INTO `sys_role_menu` VALUES (248, 2, 11);
INSERT INTO `sys_role_menu` VALUES (249, 2, 12);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` int NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `user_nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `user_gender` tinyint NOT NULL COMMENT '用户性别(女0 男1)',
  `user_token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户Token',
  `user_avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '用户头像',
  `user_age` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '用户年龄',
  `user_province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '省份',
  `user_city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '城市',
  `user_country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '地区',
  `user_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '联系电话',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (23, '测试', 1, '1111', 'https://thirdwx.qlogo.cn/mmopen/vi_32/CiaUZENzsPbWTX1GPMH9frb2Svgp6kFuBsqje95lKaB24kVDzWxVK3PMd98UsXbxia8eCKkWoq9W6VklQdMUBSWg/132', '11', '11', '11', '11', '11', '2023-05-13 23:00:00');
INSERT INTO `sys_user` VALUES (24, '微信用户', 1, 'oVPrU5PiGzwwEoefgBYoF4BZiqTo', '/image/user.png', '', '', '', '', '', '2023-05-15 13:23:23');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户角色主键ID',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, 1);
INSERT INTO `sys_user_role` VALUES (23, 22, 3);
INSERT INTO `sys_user_role` VALUES (24, 1, 2);
INSERT INTO `sys_user_role` VALUES (25, 1, 3);
INSERT INTO `sys_user_role` VALUES (26, 23, 2);
INSERT INTO `sys_user_role` VALUES (27, 1, 3);
INSERT INTO `sys_user_role` VALUES (28, 24, 3);
INSERT INTO `sys_user_role` VALUES (29, 24, 2);

SET FOREIGN_KEY_CHECKS = 1;
